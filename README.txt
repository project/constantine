-----------
Constantine
-----------

-----------
DESCRIPTION
-----------

Generally you need constants which can be used site wide. This module is the
solution of this problem. It keeps constants in separate file and avails all the
constants information at run time.

The constants are easily configurable and manageable. It does not put burden on
database for fetching constants, it keeps them in separate file and this file
is included at run time.

-----------------------
Drupal required version
-----------------------
Drupal 7.x

----------
INSTALLING
----------
1. Copy the 'constantine' folder to your sites/all/modules directory.

2. Go to admin/modules and enable the module.
Refer instalation modules at http://drupal.org/node/70151

-------------
CONFIGURATION
-------------
Go to admin/config/development/constantine. Follow instructions on this page.
