<?php

/**
 * @file
 * Includes configuration control for constantine module.
 */

/**
 * Constantine configuration form.
 */
function constantine_config_form() {
  // Set Title.
  drupal_set_title(t('constantine'));
  $form  = array();
  $form['constantine_values'] = array(
    '#type' => 'textarea',
    '#title' => 'Enter constants',
    '#description' => t("Please provide constants in key and value pair
      separated by | in new line one by one."),
    '#default_value' => variable_get('constantine_values', ''),
    '#require' => TRUE
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Submit',
    '#submit' => array('constantine_process_constants')
  );
  return $form;
}

/**
 * Get values from the forms and perform actions.
 */
function constantine_process_constants($form, &$form_state) {
  $records_info = $form_state['values']['constantine_values'];
  $records = explode("\n", $records_info);
  if (count($records) > 0) {
    $file_name = DRUPAL_ROOT . '/constantine.php';
    $file = fopen($file_name, "w");
    echo fwrite($file, "<?php\n");
    echo fwrite($file, "/**\n * @file\n * Constants Information.\n */\n");
    foreach ($records as $key => $values) {
      $record_process = explode('|', $values);
      $name = trim($record_process[0]);
      $value = trim($record_process[1]);
      $value = str_replace('"', '', $value);
      $value = str_replace("'", '', $value);
      if (!empty($name) && !empty($value)) {
        if (is_numeric($value)) {
          $current = 'define("' . $name . '", ' . $value . ');' . "\n";
          echo fwrite($file, $current);
        }
        else {
          $var_get = 'variable_get';
          if (strpos($value, $var_get) === 0) {
            $current = 'define("' . $name . '", ' . $value . ');' . "\n";
          }
          else {
            $current = 'define("' . $name . '", "' . $value . '");' . "\n";
          }
          echo fwrite($file, $current);
        }
      }
    }
    fclose($file);
  }
  variable_set('constantine_values', $records_info);
  drupal_set_message(t("Constants saved successfully."));
}
